#include <string> 

class GradeBook
{
public:
    GradeBook(std::string className, std::string teacherName);
    void setCourseName(std::string className);
    void setTeacherName(std::string teacherName);
    std::string getCourseName();
    std::string getTeacherName();
    void displayMessage();    

private:
    std::string courseName_;
    std::string proffName_;
};


#include <string>

class GradeBook 
{
public:
    GradeBook(std::string name, std::string instructorName);
    void setCourseName(std::string name);
    std::string getCourseName();
    void setInstructorName(std::string instructorName);
    std::string getInstructorName();
    void displayMessage();

private:
    std::string instructorName_;
    std::string courseName_; 
};

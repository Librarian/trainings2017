#include <iostream>

int
main()
{
    int number;

    std::cout << "Please enter any number except zero: ";
    std::cin >> number;

    if (0 == number % 2) {
        std::cout << number << " is even.\n";
        return 0;
    } 

    std::cout << number << " is odd.\n";

    return 0;
}


String is a c++ standard library. We can include it, then use it like this std::string.
For example

#include <string>
#include <iostream>

int
main()
{
    std::string myString = "someText";
    std::cout << myString << std::endl;
}


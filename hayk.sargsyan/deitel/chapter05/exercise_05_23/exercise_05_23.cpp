#include <iostream>
#include <cmath>

int
main()
{   
    for (int counterY = -4; counterY <= 4; ++counterY) {
        for (int counterX = -4; counterX <= 4; ++counterX) {
            std::cout << ((std::abs(counterX) + std::abs(counterY) <= 4) ? "*" : " "); 
        }

        std::cout << std::endl;
    }
    
    return 0;
}

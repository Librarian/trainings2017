#include <iostream>

int
main()
{
    float sum = 0;

    while (true) {
        int product;

        std::cout << "Please enter a product 1-5 (-1 to quit): ";
        std::cin >> product;

        if (-1 == product) {
            break;
        }

        int quantity;

        std::cout << "Please enter quantity of products: ";
        std::cin >> quantity;

        if (quantity < 0) {
            std::cerr << "Error 1. Quantity can't be zero or negative." << std::endl;
            return 1;
        }

        switch (product) {
            case 1: 
                sum += quantity * 2.98;
                break;
            case 2:
                sum += quantity * 4.50;
                break;
            case 3:
                sum += quantity * 9.98;
                break;
            case 4:
                sum += quantity * 4.49;
                break;
            case 5:
                sum += quantity * 6.87;
                break;
            default:
                std::cout << "Error 2. Invalid product." << std::endl;
                return 2;
        }
    }

    std::cout << "Money from sold products: " << sum << std::endl;
    
    return 0;
}



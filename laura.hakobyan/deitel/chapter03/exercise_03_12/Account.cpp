#include "Account.hpp"
#include <iostream>

Account::Account(int balance)
{
    setBalance(balance);
}

void
Account::setBalance(int balance)
{
    if(balance < 0) {
        balance_ = 0;
        std::cout << "Info 1: The initial balance was invalid." << std::cout;
        return;
    }
	
    balance_ = balance;
}

void
Account::credit(int creditAmount)
{
    balance_ = balance_ + creditAmount;
}

void
Account::debit(int debitAmount)
{
    if (debitAmount > balance_) {
        std::cout << "Info 2: Debit amount exceeded account balance." << std::cout;
        return;
    }

    balance_ = balance_ - debitAmount;
}

int
Account::getBalance()
{
    return balance_;
}


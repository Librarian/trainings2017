#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(1000);
    Account account2(500);
	
    int creditAmount1;
    std::cout << " Account 1" << std::endl;
    std::cout << "Enter amount of Credit: ";
    std::cin >> creditAmount1;
    account1.credit(creditAmount1);
    std::cout << "New Balance is: " << account1.getBalance() << std::endl;

    int debitAmount1;
    std::cout << "Enter amount of Debit: ";
    std::cin >> debitAmount1;
    account1.debit(debitAmount1);
    std::cout << "New Balance is: " << account1.getBalance() << std::endl;

    int creditAmount2;
    std::cout << " Account 2" << std::endl;
    std::cout << "Enter amount of Credit: ";
    std::cin >> creditAmount2;
    account2.credit(creditAmount2);
    std::cout << "New Balance is: " << account2.getBalance() << std::endl;

    int debitAmount2;
    std::cout << "Enter amount of Debit: ";
    std::cin >> debitAmount2;
    account2.debit(debitAmount2);
    std::cout << "New Balance is: " << account2.getBalance() << std::endl;

    return 0;
} 


#include "Employee.hpp"
#include <string>

Employee::Employee(std::string firstName, std::string lastName, int monthlySalary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setMonthlySalary(monthlySalary);
}

void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
}

std::string
Employee::getFirstName()
{
    return firstName_;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

std::string
Employee::getLastName()
{
   return lastName_;
}

void
Employee::setMonthlySalary(int monthlySalary)
{
    if (monthlySalary < 0) {
        monthlySalary_ = 0;
        return;
    }

    monthlySalary_ = monthlySalary;
}

int
Employee::getMonthlySalary()
{
    return monthlySalary_;
}


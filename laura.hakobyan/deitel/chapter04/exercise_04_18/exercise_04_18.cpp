#include <iostream>

int
main()
{
    int n = 1;
    std::cout << "N" << "\t10*N" << "\t100*N" << "\t1000*N" << std::endl;
    while (n <= 5) {
        std::cout << n << "\t" << n * 10 << "\t" << n * 100 << "\t" << n * 1000 << std::endl;
        n++;
    }
    return 0;
}

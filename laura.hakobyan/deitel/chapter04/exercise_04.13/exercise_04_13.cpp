#include <iostream>
#include <iomanip>

int
main()
{
    int miles = 0;
    int sumMiles = 0;
    int sumGallons = 0;

    while (miles != -1) {
        std::cout << "Enter miles driven (-1 to quit): ";
        std::cin >> miles;
        if (-1 == miles) {
            std::cout << miles << std::endl;
            return 0;
        }
        if (miles < 0) {
            std::cerr << "Error 1: the miles cannot be negative." << std::endl;
            return 1;
        }

        int gallons;
        std::cout << "Enter gallons used: ";
        std::cin >> gallons;
        if (gallons < 0) {
            std::cerr << "Error 2: gallons' number cannot be negative." << std::endl;
            return 2;
        }
        double tankful = static_cast<double>(miles) / gallons;
        sumMiles += miles;
        sumGallons += gallons;
        std::cout << "MPG this tankful: " << std::setprecision(6) << std::fixed << tankful << std::endl;
        double total = static_cast<double>(sumMiles) / sumGallons;
        std::cout << "Total MPG: " << std::setprecision(6) << std::fixed << total << std::endl;
    }
    return 0;
}


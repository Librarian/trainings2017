///write odd or even 
#include <iostream>
 
int
main() 
{ 
    int a;
    std::cout << "Type number: " << "\n";
    std::cin >> a;
    if (a % 2 == 0) {
        std::cout << a << " is even " << std::endl;
        return 0;    
    }
    std::cout << a << " is odd " << std::endl;
    
    return 0;
}

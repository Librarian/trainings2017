#include <iostream>

int
main()
{
    double side1, side2, side3;
    std::cout << "Enter triangles sides: ";
    std::cin >> side1 >> side2 >> side3;

    if (side1 <= 0) {
        std::cerr << "Error 1. Enter positive number." << std::endl;
        return 1;
    }
    if (side2 <= 0) {
        std::cerr << "Error 1. Enter positive number." << std::endl;
        return 1;
    }
    if (side3 <= 0) {
        std::cerr << "Error 1. Enter positive number." << std::endl;
        return 1;
    }

    if (side1 + side2 > side3) {
        if (side1 + side3 > side2) {
            if (side2 + side3 > side1) {
                std::cout << "Triangle with this sides exists." << std::endl;
                return 0;
            }
        }
    }
    std::cout << "Triangle with this sides doesn't exist." << std::endl;
    return 0;
}


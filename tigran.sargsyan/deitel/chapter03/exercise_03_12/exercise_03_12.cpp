#include "Account.hpp"

#include <iostream>

int
main()
{
    Account account1(120);
    Account account2(-100);

    int creditSum1;
    std::cout << "Account 1. Enter credit sum: ";
    std::cin >> creditSum1;
    account1.credit(creditSum1);
    std::cout << "Account 1. New Balance is: " << account1.getBalance() << std::endl;

    int debitSum1;
    std::cout << "Account 1. Enter debit sum: ";
    std::cin >> debitSum1;
    account1.debit(debitSum1);
    std::cout << "Account 1. New Balance is: " << account1.getBalance() << std::endl;

    int creditSum2;
    std::cout << "Account 2. Enter credit sum: ";
    std::cin >> creditSum2;
    account2.credit(creditSum2);
    std::cout << "Account 2. New Balance is: " << account2.getBalance() << std::endl;

    int debitSum2;
    std::cout << "Account 2. Enter debit sum: ";
    std::cin >> debitSum2;
    account2.debit(debitSum2);
    std::cout << "Account 2. New Balance is: " << account2.getBalance() << std::endl;

    return 0;
}


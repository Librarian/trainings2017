Default constructor is a constructor without parameters. If a class has no constructor, compiler calls an implicitly defined default constructor, which initializes each data members default constructor and sets default values for them (it could be a garbage value, too).


#include <iostream>

int
main()
{
    for (int line = 1; line <= 5; ++line) {
        int factorial = 1;
        std::cout << line << "!=";
        for (int column = 1; column <= line; ++column) {
            std::cout << column;
            factorial *= column;
            if (column == line) {
                break;
            }
            std::cout << "*";
        }
        std::cout << "=" << factorial << std::endl;
    }

    return 0;
}

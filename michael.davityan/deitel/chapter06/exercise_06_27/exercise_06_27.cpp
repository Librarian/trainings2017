#include <iostream>
#include <iomanip>

double celsius(const double fahrenheit);
double fahrenheit(const double celsius);

int 
main()
{
    std::cout << "Celsius to Fahrenheit table---------------------------------------------------------------------------------------\n";
    const int LINE_LIMIT_FOR_CELSIUS = 15; 
    for (int line = 0; line < LINE_LIMIT_FOR_CELSIUS; ++line) {
        const int CELSIUS_LIMIT = 100;
        for (int celsius = line; celsius <= CELSIUS_LIMIT; celsius += 15) {
            std::cout << std::setw(3) << celsius << "C - " << std::setw(5)
                      << fahrenheit(static_cast<double>(celsius)) << "F | ";
        }
        std::cout << std::endl;
    }

    std::cout << "\nFahrenheit to Celsius table--------------------------------------------------------------------------------------\n";
    const int LINE_LIMIT_FOR_FAHRENHEIT = 36;
    for (int line = 0; line < LINE_LIMIT_FOR_FAHRENHEIT; ++line) {
        const int FAHRENHEIT_LIMIT = 212;
        for (int fahrenheit = 32 + line; fahrenheit <= FAHRENHEIT_LIMIT; fahrenheit += 36) {
                std::cout << std::setw(3) <<fahrenheit << "F -" << std::setw(9)
                          << celsius(static_cast<double>(fahrenheit)) << "C | ";
        }
        std::cout << std::endl;
    }

    return 0;
}

double 
celsius(const double fahrenheit)
{
    return (fahrenheit - 32.0) * 5.0 / 9;
}

double 
fahrenheit(const double celsius)
{
    return 9.0 / 5 * celsius + 32.0;
}

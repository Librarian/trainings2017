#include <iostream>
#include <cmath>

int
main()
{
    int numberQuantity;
    std::cin >> numberQuantity;
    if (numberQuantity < 0) {
        std::cout << "Error 1: wrong quantity." << std::endl;
        return 1;
    }
    std::cout << "base\t" << "rounded\n";
    for (int counter = 1; counter <= numberQuantity; ++counter) {
        double number;
        std::cin >> number;
        int result = std::floor(number + .5);
        std::cout << number << "\t   " << result << std::endl;
    }

    return 0;
}

#include<iostream>

int
main()
{
    int number1;
    int number2;

    std::cout << "Insert two whole numbers: ";
    std::cin >> number1 >> number2;

    if (0 == number2) {
        std::cout << "Error 1: division by zero." << std::endl;
        return 1;
    }

    std::cout << "Sum is: " << (number1 + number2) << "\n"
              << "Difference is: " << (number1 - number2) << "\n"
              << "Product is: " << (number1 * number2) << "\n"
              << "Division is: " << (number1 / number2) << std::endl;
     
    return 0;
}

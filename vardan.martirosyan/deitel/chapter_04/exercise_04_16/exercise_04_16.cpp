#include <iostream>

int
main()
{
    while (true) {
        int hours;
        std::cout << "Enter hours worked (-1 to end): ";
        std::cin  >> hours;

        if (-1 == hours) {
            std::cout << std::endl;
            return 0;
        }

        if (hours < 0) {
            std::cerr << "Error 1: Hours can not be negative." << std::endl;
            return 1;
        }

        double hourlyRateOfPay;
        std::cout << "Enter hourly rate of the worker ($00.00): ";
        std::cin  >>  hourlyRateOfPay;

        double salary = (hours * hourlyRateOfPay);
        if (hours > 40) {
            salary += ((hours - 40) * (hourlyRateOfPay / 2));
        } 
        std::cout << "Salary is $" << salary << std::endl << std::endl; 
    }
    std::cerr << "Error 2: Loopp while does not work. " << std::endl;
    return 2;
}
